import naiveCommon from "@/assets/themes/naive.common.json";
import naiveDarkJson from "@/assets/themes/naive.dark.json";
import naiveLightJson from "@/assets/themes/naive.light.json";

export let naiveDark = Object.assign({ ...naiveCommon }, naiveDarkJson);
export let naiveLight = Object.assign({ ...naiveCommon }, naiveLightJson);
