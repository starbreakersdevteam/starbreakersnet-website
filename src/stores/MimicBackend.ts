export default {
  getViewerDiscordUrl: () => {
    return 'https://discordapp.com/widget?id=674668711566639114&theme=dark';
  },
  getViewerYoutubeZapUrl: () => {
    return 'https://www.youtube.com/embed/videoseries?list=PLRyncVRdYNSJIUIHfr8f5Tewxmab2BtIj';
  }
}